#!/bin/bash

while getopts ":btp" opt; do
  case $opt in
    b)
      # Build stage
      gitlab-runner exec shell build
      ;;
    t)
      # Test stage
      gitlab-runner exec shell test
      ;;
    p)
      # Package stage
      gitlab-runner exec shell package
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
  esac
done
